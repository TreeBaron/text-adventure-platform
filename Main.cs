﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Diagnostics;
using System.IO;

namespace TextAdventurePlatform
{
    public partial class Main : Form
    {

        public static Main Edit = null;

        //Variables
        Thing Selected = new Thing();
        public bool InConversation = false;

        public static string JustSaid;
        public static string Input;

        public Main()
        {
            InitializeComponent();
            Edit = this; //good god
            Start();
        }

        //Equip button, rename failed??
        private void button4_Click(object sender, EventArgs e)
        {
            if (Selected != null && Player.Things.Contains(Selected))
            {
                Player.EquippedItem = Selected;
                Main.Edit.WriteLine(Selected.name + " equipped.");
            }
            else
            {
                Main.Edit.WriteLine("There's nothing to equip.");
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }



        public void WriteLine(string a)
        {
            //Adds new line
            richTextBox1.Text += Environment.NewLine;
            richTextBox1.Text += a;
            //Moves Scrollbar down
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
        }

        public void Write(string a)
        {
            //Adds new line
            richTextBox1.Text += a;
            //Moves Scrollbar down
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
        }

        public void ClearText()
        {
            richTextBox1.Clear();
        }

        public void AddLine()
        {
            //Adds new line
            richTextBox1.Text += Environment.NewLine;
            //Moves Scrollbar down
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
        }

        public void AddLine(int a)
        {
            for (int i = 0; i < a; i++)
            {
                //Adds new line
                richTextBox1.Text += Environment.NewLine;
                //Moves Scrollbar down
                richTextBox1.SelectionStart = richTextBox1.TextLength;
                richTextBox1.ScrollToCaret();
            }
        }

        public string UserText
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        private void Start()
        {
           
            Main.Edit.WriteLine("Welcome to the Text Adventure Platform! Created By: IIandrew");
            Main.Edit.AddLine();
            Main.Edit.WriteLine("Click the \"Creator\" button to load a project. Then select \"Play Project\" to begin playing!");

        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
           ComboBoxPlayerInventory.Items.Clear();


           foreach(Thing Value in Player.Things)
           {
               ComboBoxPlayerInventory.Items.Add(Value.name);
           }
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach(Thing Value in Player.Things)
            {
                if (Value.name == ComboBoxPlayerInventory.SelectedItem.ToString())
                {
                    Selected = Value;
                }
            }
        }

        private void DropButton_Click(object sender, EventArgs e)
        {
            if (Selected != null)
            {
                ComboBoxPlayerInventory.Text = "";
                Player.DropThing(Selected);
                Selected = null;
            }
            else
            {
                Main.Edit.WriteLine("There's nothing to drop.");
            }
        }

        private void UseButton_Click(object sender, EventArgs e)
        {
            if (Selected != null)
            {
                Selected.Use(Selected);
            }
            else
            {
                Main.Edit.WriteLine("There's nothing to use.");
            }
        }

        private void InspectButton_Click(object sender, EventArgs e)
        {
            if (Selected != null)
            {
                Main.Edit.WriteLine("You look down at the " + Selected.name + ". " + Selected.description);
            }
            else
            {
                Main.Edit.WriteLine("You stare off into the void. What is this thing we call nothing you wonder, as you fondle it in your hands.");
            }
        }

        private void GoNorthButton_Click(object sender, EventArgs e)
        {
            try
            {
                Player.GoNorth();
            }
            catch
            {

            }
        }

        private void WaitButton_Click(object sender, EventArgs e)
        {
            try
            {
                Player.Wait();
            }
            catch
            {

            }
        }

        private void EastButton_Click(object sender, EventArgs e)
        {
            try
            {
                Player.GoEast();
            }
            catch
            {

            }
        }

        private void WestButton_Click(object sender, EventArgs e)
        {
            try
            {
                Player.GoWest();
            }
            catch
            {

            }
        }

        private void SouthButton_Click(object sender, EventArgs e)
        {
            try
            {
                Player.GoSouth();
            }
            catch
            {

            }
        }

        private void LookButton_Click(object sender, EventArgs e)
        {
            if (Player.CurrentRoom != null)
            {
                Player.CurrentRoom.DescribePlace();

                foreach (Room Value in Map.Rooms)
                {
                    if (Value.Coord.X == Player.Coord.X + 1)
                    {
                        Main.Edit.WriteLine("To the east you see " + Value.name + ".");
                    }
                    if (Value.Coord.X == Player.Coord.X - 1)
                    {
                        Main.Edit.WriteLine("To the west you see " + Value.name + ".");
                    }
                    if (Value.Coord.Y == Player.Coord.Y + 1)
                    {
                        Main.Edit.WriteLine("To the north you see " + Value.name + ".");
                    }
                    if (Value.Coord.Y == Player.Coord.Y - 1)
                    {
                        Main.Edit.WriteLine("To the south you see " + Value.name + ".");
                    }
                }
            }
            else
            {
                Main.Edit.WriteLine("You stare into the void, and contemplate the meaning of it all.");
            }
            
        }

        private void SayButton_Click(object sender, EventArgs e)
        {
            if (Player.CurrentRoom != null)
            {
                JustSaid = textBox1.Text;
                Input = textBox1.Text.ToLower();
                textBox1.Clear();

                if (InConversation == false)
                {
                    Person SelectedPerson = new Person();

                    foreach (Person p in Player.CurrentRoom.People)
                    {
                        if (Input.Contains(p.name.ToLower()))
                        {
                            Main.Edit.AddLine();
                            Main.Edit.WriteLine("You try to speak with "+p.name+":");
                            for (int i = 0; i < p.Questions.Count; i++)
                            {
                                Main.Edit.WriteLine("[" + (i+1) + "] - " + p.Questions[i]);

                                if (i == p.Questions.Count-1)
                                {
                                    Main.Edit.WriteLine("["+(i+2)+"] - End Conversation");
                                }
                            }
                            InConversation = true;
                        }
                    }
                }
                else
                {
                    int holder = -1;
                    foreach (Person p in Player.CurrentRoom.People)
                    {
                        //If there's questions and answers
                        if (p.Answers.Count > 0 && p.Questions.Count > 0)
                        {
                            //Loop through question count +1 for non zero ui
                            for (int i = 0; i < p.Questions.Count+1; i++)
                            {
                                //Check if input contains this number
                                if (Input.Contains(i.ToString()))
                                {
                                    holder = i-1;
                                }
                                
                            }


                            if (holder == -1)
                            {
                                Main.Edit.WriteLine("Goodbye "+p.name+".");
                                InConversation = false;
                                return;
                            }
                           

                            Main.Edit.WriteLine(holder.ToString());
                            Main.Edit.WriteLine(p.Answers[holder]);

                        }
                    }
                }
            }
        }

        private void ComboBoxRoomItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach(Thing thing in Player.CurrentRoom.Things)
            {
                if (thing.name == ComboBoxRoomItems.SelectedItem.ToString())
                {
                    Selected = thing;
                }
            }
        }

        private void ComboBoxRoomItems_DropDown(object sender, EventArgs e)
        {
            if (Player.CurrentRoom != null)
            {
                ComboBoxRoomItems.Items.Clear();

                foreach (Thing Value in Player.CurrentRoom.Things)
                {
                    ComboBoxRoomItems.Items.Add(Value.name);
                }
            }
        }

        private void TakeButton_Click(object sender, EventArgs e)
        {
            if (Selected != null)
            {
                Player.TakeThing(Selected);
                ComboBoxRoomItems.Items.Clear();
                ComboBoxRoomItems.Text = "";
                Selected = null;
            }
            else
            {
                Main.Edit.WriteLine("You cannot take that.");
            }
        }

        private void LoadGameButton_Click(object sender, EventArgs e)
        {
            if (File.Exists("data.bin"))
            {
                Map.Load();

                Main.Edit.ClearText();

                if (Player.CurrentRoom != null)
                {
                    Main.Edit.WriteLine(Player.CurrentRoom.name);
                    Player.CurrentRoom.DescribePlace();
                }
            }
        }

        private void SaveGameButton_Click(object sender, EventArgs e)
        {
                Map.Save();
        }

        private void CreatorButton_Click(object sender, EventArgs e)
        {
            ProjectForm NewForm = new ProjectForm();

            NewForm.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
