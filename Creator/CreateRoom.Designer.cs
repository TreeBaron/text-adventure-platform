﻿namespace TextAdventurePlatform
{
    partial class CreateRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateRoom));
            this.LoadRoomComboBox = new System.Windows.Forms.ComboBox();
            this.LoadRoomButton = new System.Windows.Forms.Button();
            this.DeleteRoomButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SayOnFirstEntranceTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.IsLockedCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.XCoordTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.YCoordTextBox = new System.Windows.Forms.TextBox();
            this.PersonToDieComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PersonToGiveDialogueToComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.QuestionTextBox = new System.Windows.Forms.TextBox();
            this.AnswerTextBox = new System.Windows.Forms.TextBox();
            this.RemoveDialogueComboBox = new System.Windows.Forms.ComboBox();
            this.RemoveDialogueButton = new System.Windows.Forms.Button();
            this.ProjectPeopleThingComboBox = new System.Windows.Forms.ComboBox();
            this.AddPersonThingButton = new System.Windows.Forms.Button();
            this.RoomInventoryComboBox = new System.Windows.Forms.ComboBox();
            this.RemovePersonThingButton = new System.Windows.Forms.Button();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.AddDialogueButton = new System.Windows.Forms.Button();
            this.ProjectKeyComboBox = new System.Windows.Forms.ComboBox();
            this.AddKeyButton = new System.Windows.Forms.Button();
            this.RemoveKeyButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LoadRoomComboBox
            // 
            this.LoadRoomComboBox.FormattingEnabled = true;
            this.LoadRoomComboBox.Location = new System.Drawing.Point(13, 13);
            this.LoadRoomComboBox.Name = "LoadRoomComboBox";
            this.LoadRoomComboBox.Size = new System.Drawing.Size(380, 21);
            this.LoadRoomComboBox.TabIndex = 0;
            this.LoadRoomComboBox.DropDown += new System.EventHandler(this.LoadRoomComboBox_DropDown);
            this.LoadRoomComboBox.SelectedIndexChanged += new System.EventHandler(this.LoadRoomComboBox_SelectedIndexChanged);
            // 
            // LoadRoomButton
            // 
            this.LoadRoomButton.BackColor = System.Drawing.Color.Black;
            this.LoadRoomButton.Location = new System.Drawing.Point(13, 41);
            this.LoadRoomButton.Name = "LoadRoomButton";
            this.LoadRoomButton.Size = new System.Drawing.Size(181, 23);
            this.LoadRoomButton.TabIndex = 1;
            this.LoadRoomButton.Text = "Load Room";
            this.LoadRoomButton.UseVisualStyleBackColor = false;
            this.LoadRoomButton.Click += new System.EventHandler(this.LoadRoomButton_Click);
            // 
            // DeleteRoomButton
            // 
            this.DeleteRoomButton.BackColor = System.Drawing.Color.Black;
            this.DeleteRoomButton.Location = new System.Drawing.Point(200, 41);
            this.DeleteRoomButton.Name = "DeleteRoomButton";
            this.DeleteRoomButton.Size = new System.Drawing.Size(193, 23);
            this.DeleteRoomButton.TabIndex = 2;
            this.DeleteRoomButton.Text = "Delete Room";
            this.DeleteRoomButton.UseVisualStyleBackColor = false;
            this.DeleteRoomButton.Click += new System.EventHandler(this.DeleteRoomButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(13, 92);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(380, 20);
            this.NameTextBox.TabIndex = 4;
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(12, 135);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(381, 20);
            this.DescriptionTextBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Description";
            // 
            // SayOnFirstEntranceTextBox
            // 
            this.SayOnFirstEntranceTextBox.Location = new System.Drawing.Point(13, 174);
            this.SayOnFirstEntranceTextBox.Name = "SayOnFirstEntranceTextBox";
            this.SayOnFirstEntranceTextBox.Size = new System.Drawing.Size(381, 20);
            this.SayOnFirstEntranceTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Say On First Entrance";
            // 
            // IsLockedCheckBox
            // 
            this.IsLockedCheckBox.AutoSize = true;
            this.IsLockedCheckBox.Location = new System.Drawing.Point(12, 205);
            this.IsLockedCheckBox.Name = "IsLockedCheckBox";
            this.IsLockedCheckBox.Size = new System.Drawing.Size(73, 17);
            this.IsLockedCheckBox.TabIndex = 9;
            this.IsLockedCheckBox.Text = "Is Locked";
            this.IsLockedCheckBox.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "X Coordinate";
            // 
            // XCoordTextBox
            // 
            this.XCoordTextBox.Location = new System.Drawing.Point(12, 246);
            this.XCoordTextBox.Name = "XCoordTextBox";
            this.XCoordTextBox.Size = new System.Drawing.Size(182, 20);
            this.XCoordTextBox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(197, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Y Coordinate";
            // 
            // YCoordTextBox
            // 
            this.YCoordTextBox.Location = new System.Drawing.Point(200, 246);
            this.YCoordTextBox.Name = "YCoordTextBox";
            this.YCoordTextBox.Size = new System.Drawing.Size(197, 20);
            this.YCoordTextBox.TabIndex = 13;
            // 
            // PersonToDieComboBox
            // 
            this.PersonToDieComboBox.FormattingEnabled = true;
            this.PersonToDieComboBox.Location = new System.Drawing.Point(12, 288);
            this.PersonToDieComboBox.Name = "PersonToDieComboBox";
            this.PersonToDieComboBox.Size = new System.Drawing.Size(381, 21);
            this.PersonToDieComboBox.TabIndex = 14;
            this.PersonToDieComboBox.DropDown += new System.EventHandler(this.PersonToDieComboBox_DropDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(182, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Person To Die Upon Player Entrance";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 316);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Person To Give New Dialogue To";
            // 
            // PersonToGiveDialogueToComboBox
            // 
            this.PersonToGiveDialogueToComboBox.FormattingEnabled = true;
            this.PersonToGiveDialogueToComboBox.Location = new System.Drawing.Point(12, 333);
            this.PersonToGiveDialogueToComboBox.Name = "PersonToGiveDialogueToComboBox";
            this.PersonToGiveDialogueToComboBox.Size = new System.Drawing.Size(381, 21);
            this.PersonToGiveDialogueToComboBox.TabIndex = 17;
            this.PersonToGiveDialogueToComboBox.DropDown += new System.EventHandler(this.PersonToGiveDialogueToComboBox_DropDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 361);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Question";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 401);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Answer";
            // 
            // QuestionTextBox
            // 
            this.QuestionTextBox.Location = new System.Drawing.Point(12, 378);
            this.QuestionTextBox.Name = "QuestionTextBox";
            this.QuestionTextBox.Size = new System.Drawing.Size(381, 20);
            this.QuestionTextBox.TabIndex = 20;
            // 
            // AnswerTextBox
            // 
            this.AnswerTextBox.Location = new System.Drawing.Point(12, 417);
            this.AnswerTextBox.Name = "AnswerTextBox";
            this.AnswerTextBox.Size = new System.Drawing.Size(381, 20);
            this.AnswerTextBox.TabIndex = 21;
            // 
            // RemoveDialogueComboBox
            // 
            this.RemoveDialogueComboBox.FormattingEnabled = true;
            this.RemoveDialogueComboBox.Location = new System.Drawing.Point(12, 468);
            this.RemoveDialogueComboBox.Name = "RemoveDialogueComboBox";
            this.RemoveDialogueComboBox.Size = new System.Drawing.Size(381, 21);
            this.RemoveDialogueComboBox.TabIndex = 22;
            this.RemoveDialogueComboBox.DropDown += new System.EventHandler(this.RemoveDialogueComboBox_DropDown);
            this.RemoveDialogueComboBox.SelectedIndexChanged += new System.EventHandler(this.RemoveDialogueComboBox_SelectedIndexChanged);
            // 
            // RemoveDialogueButton
            // 
            this.RemoveDialogueButton.BackColor = System.Drawing.Color.Black;
            this.RemoveDialogueButton.Location = new System.Drawing.Point(12, 495);
            this.RemoveDialogueButton.Name = "RemoveDialogueButton";
            this.RemoveDialogueButton.Size = new System.Drawing.Size(381, 23);
            this.RemoveDialogueButton.TabIndex = 23;
            this.RemoveDialogueButton.Text = "Remove Dialogue";
            this.RemoveDialogueButton.UseVisualStyleBackColor = false;
            this.RemoveDialogueButton.Click += new System.EventHandler(this.RemoveDialogueButton_Click);
            // 
            // ProjectPeopleThingComboBox
            // 
            this.ProjectPeopleThingComboBox.FormattingEnabled = true;
            this.ProjectPeopleThingComboBox.Location = new System.Drawing.Point(12, 524);
            this.ProjectPeopleThingComboBox.Name = "ProjectPeopleThingComboBox";
            this.ProjectPeopleThingComboBox.Size = new System.Drawing.Size(381, 21);
            this.ProjectPeopleThingComboBox.TabIndex = 24;
            this.ProjectPeopleThingComboBox.DropDown += new System.EventHandler(this.ProjectPeopleThingComboBox_DropDown);
            this.ProjectPeopleThingComboBox.SelectedIndexChanged += new System.EventHandler(this.ProjectPeopleThingComboBox_SelectedIndexChanged);
            // 
            // AddPersonThingButton
            // 
            this.AddPersonThingButton.BackColor = System.Drawing.Color.Black;
            this.AddPersonThingButton.Location = new System.Drawing.Point(12, 551);
            this.AddPersonThingButton.Name = "AddPersonThingButton";
            this.AddPersonThingButton.Size = new System.Drawing.Size(381, 23);
            this.AddPersonThingButton.TabIndex = 25;
            this.AddPersonThingButton.Text = "Add Person/Thing";
            this.AddPersonThingButton.UseVisualStyleBackColor = false;
            this.AddPersonThingButton.Click += new System.EventHandler(this.AddPersonThingButton_Click);
            // 
            // RoomInventoryComboBox
            // 
            this.RoomInventoryComboBox.FormattingEnabled = true;
            this.RoomInventoryComboBox.Location = new System.Drawing.Point(13, 580);
            this.RoomInventoryComboBox.Name = "RoomInventoryComboBox";
            this.RoomInventoryComboBox.Size = new System.Drawing.Size(380, 21);
            this.RoomInventoryComboBox.TabIndex = 26;
            this.RoomInventoryComboBox.DropDown += new System.EventHandler(this.RoomInventoryComboBox_DropDown);
            this.RoomInventoryComboBox.SelectedIndexChanged += new System.EventHandler(this.RoomInventoryComboBox_SelectedIndexChanged);
            // 
            // RemovePersonThingButton
            // 
            this.RemovePersonThingButton.BackColor = System.Drawing.Color.Black;
            this.RemovePersonThingButton.Location = new System.Drawing.Point(13, 607);
            this.RemovePersonThingButton.Name = "RemovePersonThingButton";
            this.RemovePersonThingButton.Size = new System.Drawing.Size(380, 23);
            this.RemovePersonThingButton.TabIndex = 27;
            this.RemovePersonThingButton.Text = "Remove Person/Thing";
            this.RemovePersonThingButton.UseVisualStyleBackColor = false;
            this.RemovePersonThingButton.Click += new System.EventHandler(this.RemovePersonThingButton_Click);
            // 
            // SubmitButton
            // 
            this.SubmitButton.BackColor = System.Drawing.Color.Black;
            this.SubmitButton.Location = new System.Drawing.Point(12, 702);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(381, 23);
            this.SubmitButton.TabIndex = 28;
            this.SubmitButton.Text = "Submit";
            this.SubmitButton.UseVisualStyleBackColor = false;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // AddDialogueButton
            // 
            this.AddDialogueButton.BackColor = System.Drawing.Color.Black;
            this.AddDialogueButton.Location = new System.Drawing.Point(12, 439);
            this.AddDialogueButton.Name = "AddDialogueButton";
            this.AddDialogueButton.Size = new System.Drawing.Size(381, 23);
            this.AddDialogueButton.TabIndex = 29;
            this.AddDialogueButton.Text = "Add Dialogue";
            this.AddDialogueButton.UseVisualStyleBackColor = false;
            this.AddDialogueButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // ProjectKeyComboBox
            // 
            this.ProjectKeyComboBox.FormattingEnabled = true;
            this.ProjectKeyComboBox.Location = new System.Drawing.Point(12, 636);
            this.ProjectKeyComboBox.Name = "ProjectKeyComboBox";
            this.ProjectKeyComboBox.Size = new System.Drawing.Size(381, 21);
            this.ProjectKeyComboBox.TabIndex = 30;
            this.ProjectKeyComboBox.DropDown += new System.EventHandler(this.ProjectKeyComboBox_DropDown);
            this.ProjectKeyComboBox.SelectedIndexChanged += new System.EventHandler(this.ProjectKeyComboBox_SelectedIndexChanged);
            // 
            // AddKeyButton
            // 
            this.AddKeyButton.Location = new System.Drawing.Point(12, 663);
            this.AddKeyButton.Name = "AddKeyButton";
            this.AddKeyButton.Size = new System.Drawing.Size(182, 23);
            this.AddKeyButton.TabIndex = 31;
            this.AddKeyButton.Text = "Add Key";
            this.AddKeyButton.UseVisualStyleBackColor = true;
            this.AddKeyButton.Click += new System.EventHandler(this.AddKeyButton_Click);
            // 
            // RemoveKeyButton
            // 
            this.RemoveKeyButton.Location = new System.Drawing.Point(200, 663);
            this.RemoveKeyButton.Name = "RemoveKeyButton";
            this.RemoveKeyButton.Size = new System.Drawing.Size(193, 23);
            this.RemoveKeyButton.TabIndex = 32;
            this.RemoveKeyButton.Text = "Remove Key";
            this.RemoveKeyButton.UseVisualStyleBackColor = true;
            this.RemoveKeyButton.Click += new System.EventHandler(this.RemoveKeyButton_Click);
            // 
            // CreateRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(405, 738);
            this.Controls.Add(this.RemoveKeyButton);
            this.Controls.Add(this.AddKeyButton);
            this.Controls.Add(this.ProjectKeyComboBox);
            this.Controls.Add(this.AddDialogueButton);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.RemovePersonThingButton);
            this.Controls.Add(this.RoomInventoryComboBox);
            this.Controls.Add(this.AddPersonThingButton);
            this.Controls.Add(this.ProjectPeopleThingComboBox);
            this.Controls.Add(this.RemoveDialogueButton);
            this.Controls.Add(this.RemoveDialogueComboBox);
            this.Controls.Add(this.AnswerTextBox);
            this.Controls.Add(this.QuestionTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.PersonToGiveDialogueToComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.PersonToDieComboBox);
            this.Controls.Add(this.YCoordTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.XCoordTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IsLockedCheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SayOnFirstEntranceTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeleteRoomButton);
            this.Controls.Add(this.LoadRoomButton);
            this.Controls.Add(this.LoadRoomComboBox);
            this.ForeColor = System.Drawing.Color.Lime;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CreateRoom";
            this.Text = "CreateRoom";
            this.Load += new System.EventHandler(this.CreateRoom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox LoadRoomComboBox;
        private System.Windows.Forms.Button LoadRoomButton;
        private System.Windows.Forms.Button DeleteRoomButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SayOnFirstEntranceTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox IsLockedCheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox XCoordTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox YCoordTextBox;
        private System.Windows.Forms.ComboBox PersonToDieComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox PersonToGiveDialogueToComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox QuestionTextBox;
        private System.Windows.Forms.TextBox AnswerTextBox;
        private System.Windows.Forms.ComboBox RemoveDialogueComboBox;
        private System.Windows.Forms.Button RemoveDialogueButton;
        private System.Windows.Forms.ComboBox ProjectPeopleThingComboBox;
        private System.Windows.Forms.Button AddPersonThingButton;
        private System.Windows.Forms.ComboBox RoomInventoryComboBox;
        private System.Windows.Forms.Button RemovePersonThingButton;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.Button AddDialogueButton;
        private System.Windows.Forms.ComboBox ProjectKeyComboBox;
        private System.Windows.Forms.Button AddKeyButton;
        private System.Windows.Forms.Button RemoveKeyButton;
    }
}