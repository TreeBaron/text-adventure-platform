﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextAdventurePlatform
{
    public partial class ProjectForm : Form
    {
        //Variables
        string filepath = Environment.SpecialFolder.MyDocuments.ToString();

        public ProjectForm()
        {
            InitializeComponent();
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            
            CreatePerson CP = new CreatePerson();

            CP.Show();
        }

        private void CreateObjectButton_Click(object sender, EventArgs e)
        {
            CreateObject CO = new CreateObject();
            CO.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CreateRoom CR = new CreateRoom(); 
            CR.Show();
        }

        private void SaveProjectButton_Click(object sender, EventArgs e)
        {
  
            saveFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            saveFileDialog1.DefaultExt = ".bin";
            saveFileDialog1.Filter = "Binary Files | *.bin";
            saveFileDialog1.ShowDialog();
            

            if (saveFileDialog1.FileName != "")
            {
                Project.FileName = saveFileDialog1.FileName;
                Project.FilePath = System.IO.Path.GetDirectoryName(filepath);
            }
            
            Project.SaveProject();
        }

        private void LoadProjectButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            openFileDialog1.ShowDialog();

            if (openFileDialog1.FileName != "")
            {
                Project.FileName = openFileDialog1.FileName;
                Project.FilePath = System.IO.Path.GetDirectoryName(Project.FileName);
            }

            Project.LoadProject();
        }

        private void PlayProject_Click(object sender, EventArgs e)
        {
            if (Project.Rooms.Count > 0)
            {
                Map.Rooms = Project.Rooms;
                Player.Turn = 0;
                Player.Health = 100;
                Player.EquippedItem = null;
                Player.Things.Clear();
                Player.UpdateRoom();

                Main.Edit.ClearText();

                Main.Edit.WriteLine(Player.CurrentRoom.name);
                Player.CurrentRoom.DescribePlace();

                //Close this form
                this.Close();
            }
            else
            {
                MessageBox.Show("There are no rooms in your project to load!");
            }
        }

    }
}
