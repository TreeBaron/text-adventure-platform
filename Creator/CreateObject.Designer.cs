﻿namespace TextAdventurePlatform
{
    partial class CreateObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateObject));
            this.LoadObjectComboBox = new System.Windows.Forms.ComboBox();
            this.LoadObjectButton = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.WeaponCheckBox = new System.Windows.Forms.CheckBox();
            this.ContainerCheckBox = new System.Windows.Forms.CheckBox();
            this.SetKeyComboBox = new System.Windows.Forms.ComboBox();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.AddObjectToContainerButton = new System.Windows.Forms.Button();
            this.AddObjectComboBox = new System.Windows.Forms.ComboBox();
            this.RemoveObjectFromContainerButton = new System.Windows.Forms.Button();
            this.ContainerItemsComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LoadObjectComboBox
            // 
            this.LoadObjectComboBox.FormattingEnabled = true;
            this.LoadObjectComboBox.Location = new System.Drawing.Point(16, 11);
            this.LoadObjectComboBox.Name = "LoadObjectComboBox";
            this.LoadObjectComboBox.Size = new System.Drawing.Size(372, 21);
            this.LoadObjectComboBox.TabIndex = 0;
            this.LoadObjectComboBox.DropDown += new System.EventHandler(this.LoadObjectComboBox_DropDown);
            this.LoadObjectComboBox.SelectedIndexChanged += new System.EventHandler(this.LoadObjectComboBox_SelectedIndexChanged);
            // 
            // LoadObjectButton
            // 
            this.LoadObjectButton.BackColor = System.Drawing.Color.Black;
            this.LoadObjectButton.Location = new System.Drawing.Point(16, 39);
            this.LoadObjectButton.Name = "LoadObjectButton";
            this.LoadObjectButton.Size = new System.Drawing.Size(372, 23);
            this.LoadObjectButton.TabIndex = 1;
            this.LoadObjectButton.Text = "Load Object";
            this.LoadObjectButton.UseVisualStyleBackColor = false;
            this.LoadObjectButton.Click += new System.EventHandler(this.LoadObjectButton_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(16, 114);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(35, 13);
            this.NameLabel.TabIndex = 2;
            this.NameLabel.Text = "Name";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(16, 130);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(372, 20);
            this.NameTextBox.TabIndex = 3;
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(16, 175);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(372, 20);
            this.DescriptionTextBox.TabIndex = 5;
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.Location = new System.Drawing.Point(13, 158);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.DescriptionLabel.TabIndex = 4;
            this.DescriptionLabel.Text = "Description";
            // 
            // WeaponCheckBox
            // 
            this.WeaponCheckBox.AutoSize = true;
            this.WeaponCheckBox.BackColor = System.Drawing.Color.Black;
            this.WeaponCheckBox.Location = new System.Drawing.Point(16, 202);
            this.WeaponCheckBox.Name = "WeaponCheckBox";
            this.WeaponCheckBox.Size = new System.Drawing.Size(112, 17);
            this.WeaponCheckBox.TabIndex = 6;
            this.WeaponCheckBox.Text = "Object Is Weapon";
            this.WeaponCheckBox.UseVisualStyleBackColor = false;
            // 
            // ContainerCheckBox
            // 
            this.ContainerCheckBox.AutoSize = true;
            this.ContainerCheckBox.BackColor = System.Drawing.Color.Black;
            this.ContainerCheckBox.Location = new System.Drawing.Point(16, 226);
            this.ContainerCheckBox.Name = "ContainerCheckBox";
            this.ContainerCheckBox.Size = new System.Drawing.Size(116, 17);
            this.ContainerCheckBox.TabIndex = 7;
            this.ContainerCheckBox.Text = "Object Is Container";
            this.ContainerCheckBox.UseVisualStyleBackColor = false;
            // 
            // SetKeyComboBox
            // 
            this.SetKeyComboBox.FormattingEnabled = true;
            this.SetKeyComboBox.Location = new System.Drawing.Point(16, 278);
            this.SetKeyComboBox.Name = "SetKeyComboBox";
            this.SetKeyComboBox.Size = new System.Drawing.Size(369, 21);
            this.SetKeyComboBox.TabIndex = 8;
            this.SetKeyComboBox.DropDown += new System.EventHandler(this.SetKeyComboBox_DropDown);
            // 
            // SubmitButton
            // 
            this.SubmitButton.BackColor = System.Drawing.Color.Black;
            this.SubmitButton.Location = new System.Drawing.Point(16, 455);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(369, 23);
            this.SubmitButton.TabIndex = 10;
            this.SubmitButton.Text = "Submit";
            this.SubmitButton.UseVisualStyleBackColor = false;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // AddObjectToContainerButton
            // 
            this.AddObjectToContainerButton.BackColor = System.Drawing.Color.Black;
            this.AddObjectToContainerButton.Location = new System.Drawing.Point(16, 352);
            this.AddObjectToContainerButton.Name = "AddObjectToContainerButton";
            this.AddObjectToContainerButton.Size = new System.Drawing.Size(369, 23);
            this.AddObjectToContainerButton.TabIndex = 12;
            this.AddObjectToContainerButton.Text = "Add Object To Container";
            this.AddObjectToContainerButton.UseVisualStyleBackColor = false;
            this.AddObjectToContainerButton.Click += new System.EventHandler(this.AddObjectToContainerButton_Click);
            // 
            // AddObjectComboBox
            // 
            this.AddObjectComboBox.FormattingEnabled = true;
            this.AddObjectComboBox.Location = new System.Drawing.Point(16, 324);
            this.AddObjectComboBox.Name = "AddObjectComboBox";
            this.AddObjectComboBox.Size = new System.Drawing.Size(369, 21);
            this.AddObjectComboBox.TabIndex = 11;
            this.AddObjectComboBox.DropDown += new System.EventHandler(this.AddObjectComboBox_DropDown);
            // 
            // RemoveObjectFromContainerButton
            // 
            this.RemoveObjectFromContainerButton.BackColor = System.Drawing.Color.Black;
            this.RemoveObjectFromContainerButton.Location = new System.Drawing.Point(16, 411);
            this.RemoveObjectFromContainerButton.Name = "RemoveObjectFromContainerButton";
            this.RemoveObjectFromContainerButton.Size = new System.Drawing.Size(369, 23);
            this.RemoveObjectFromContainerButton.TabIndex = 14;
            this.RemoveObjectFromContainerButton.Text = "Remove Object From Container";
            this.RemoveObjectFromContainerButton.UseVisualStyleBackColor = false;
            this.RemoveObjectFromContainerButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // ContainerItemsComboBox
            // 
            this.ContainerItemsComboBox.Location = new System.Drawing.Point(16, 384);
            this.ContainerItemsComboBox.Name = "ContainerItemsComboBox";
            this.ContainerItemsComboBox.Size = new System.Drawing.Size(369, 21);
            this.ContainerItemsComboBox.TabIndex = 16;
            this.ContainerItemsComboBox.DropDown += new System.EventHandler(this.ContainerItemsComboBox_DropDown_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 257);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Key To Open Container With";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(16, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(372, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Delete Object";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // CreateObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(393, 490);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RemoveObjectFromContainerButton);
            this.Controls.Add(this.ContainerItemsComboBox);
            this.Controls.Add(this.AddObjectToContainerButton);
            this.Controls.Add(this.AddObjectComboBox);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.SetKeyComboBox);
            this.Controls.Add(this.ContainerCheckBox);
            this.Controls.Add(this.WeaponCheckBox);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.LoadObjectButton);
            this.Controls.Add(this.LoadObjectComboBox);
            this.ForeColor = System.Drawing.Color.Lime;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CreateObject";
            this.Text = "Create Object";
            this.Load += new System.EventHandler(this.CreateObject_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox LoadObjectComboBox;
        private System.Windows.Forms.Button LoadObjectButton;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.CheckBox WeaponCheckBox;
        private System.Windows.Forms.CheckBox ContainerCheckBox;
        private System.Windows.Forms.ComboBox SetKeyComboBox;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.Button AddObjectToContainerButton;
        private System.Windows.Forms.ComboBox AddObjectComboBox;
        private System.Windows.Forms.Button RemoveObjectFromContainerButton;
        private System.Windows.Forms.ComboBox ContainerItemsComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}