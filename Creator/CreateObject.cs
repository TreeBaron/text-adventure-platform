﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextAdventurePlatform
{
    public partial class CreateObject : Form
    {
        Thing SelectedThing = new Thing();

        public CreateObject()
        {
            InitializeComponent();
        }

        private void LoadObjectComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void LoadObjectComboBox_DropDown(object sender, EventArgs e)
        {
            LoadObjectComboBox.Items.Clear();

            foreach(Thing obj in Project.Things)
            {
                LoadObjectComboBox.Items.Add(obj.name);
            }
        }

        private void AddObjectToContainerButton_Click(object sender, EventArgs e)
        {
            foreach(Thing thing in Project.Things)
            {
                if (thing.name == AddObjectComboBox.Text)
                {
                    SelectedThing.Things.Add(thing);
                }
            }
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            Thing Temp = new Thing();

            Temp.name = NameTextBox.Text;

            if (Temp.name == "nothing")
            {
                MessageBox.Show("You cannot create nothing! Chanching name to something.");
                Temp.name = "something";
            }

            Temp.description = DescriptionTextBox.Text;

            if (WeaponCheckBox.Checked == true)
            {
                Temp.weapon = true;
            }
            else
            {
                Temp.weapon = false;
            }

            if (ContainerCheckBox.Checked == true)
            {
                Temp.container = true;
            }
            else
            {
                Temp.container = false;
            }

            foreach(Thing thing in Project.Things)
            {
                if (thing.name == SetKeyComboBox.SelectedText)
                {
                    Temp.keyobject = thing;
                }
            }

            if (SelectedThing.container == true)
            {
                foreach(string text in AddObjectComboBox.Items)
                {
                    foreach(Thing thing in Project.Things)
                    {
                        if (thing.name == text)
                        {
                            Temp.Things.Add(thing);
                        }
                    }
                }
            }

            foreach(Thing thing in Project.Things)
            {
                if (thing.name == Temp.name)
                {
                    Project.Things.Remove(thing);
                }
            }

                Project.Things.Add(Temp);
                MessageBox.Show("I have created a new object and added it to the project!");

        }

        private void CreateObject_Load(object sender, EventArgs e)
        {

        }

        private void LoadObjectButton_Click(object sender, EventArgs e)
        {
            foreach (Thing obj in Project.Things)
            {
                if (obj.name == LoadObjectComboBox.Text)
                {
                   // MessageBox.Show("Found matching object " + obj.name + "=" + LoadObjectComboBox.Text);

                    SelectedThing = obj;

                    NameTextBox.Text = SelectedThing.name;
                    DescriptionTextBox.Text = SelectedThing.description;

                    if (SelectedThing.weapon == false)
                    {
                        WeaponCheckBox.Checked = false;
                    }
                    else
                    {
                        WeaponCheckBox.Checked = true;
                    }

                    if (SelectedThing.container == false)
                    {
                        ContainerCheckBox.Checked = false;
                    }
                    else
                    {
                        ContainerCheckBox.Checked = true;
                    }

                    foreach (Thing thing in SelectedThing.Things)
                    {
                        ContainerItemsComboBox.Items.Add(thing.name);
                    }

                }
            }
        }

        private void SetKeyComboBox_DropDown(object sender, EventArgs e)
        {
            foreach(Thing thing in Project.Things)
            {
                SetKeyComboBox.Items.Add(thing.name);
            }
        }

        private void SetKeyButton_Click(object sender, EventArgs e)
        {
           
        }

        private void ContainerItemsComboBox_DropDown(object sender, EventArgs e)
        {
            foreach(Thing thing in SelectedThing.Things)
            {
                ContainerItemsComboBox.Items.Add(thing.name);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (SelectedThing.Things.Count > 0)
            {
                foreach (Thing thing in SelectedThing.Things)
                {
                    if (thing.name == ContainerItemsComboBox.Text)
                    {
                        SelectedThing.Things.Remove(thing);
                        ContainerItemsComboBox.Text = "";
                        break;
                    }
                }
            }
        }

        private void AddObjectComboBox_DropDown(object sender, EventArgs e)
        {
            AddObjectComboBox.Items.Clear();

            foreach (Thing obj in Project.Things)
            {
                AddObjectComboBox.Items.Add(obj.name);
            }
        }

        private void ContainerItemsComboBox_DropDown_1(object sender, EventArgs e)
        {
            ContainerItemsComboBox.Items.Clear();

            foreach(Thing obj in SelectedThing.Things)
            {
                ContainerItemsComboBox.Items.Add(obj.name);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //Delete Object Button

            foreach(Thing thing in Project.Things)
            {
                if (thing.name == LoadObjectComboBox.Text)
                {
                    Project.Things.Remove(thing);
                    LoadObjectComboBox.Text = "";
                    MessageBox.Show(thing.name + " deleted.");
                    break;
                }
            }

        }


    }
}
