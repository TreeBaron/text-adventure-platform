﻿namespace TextAdventurePlatform
{
    partial class ProjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectForm));
            this.button1 = new System.Windows.Forms.Button();
            this.CreateObjectButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.LoadProjectButton = new System.Windows.Forms.Button();
            this.SaveProjectButton = new System.Windows.Forms.Button();
            this.PlayProject = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(12, 103);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(267, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Create/Edit Person";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CreateObjectButton
            // 
            this.CreateObjectButton.BackColor = System.Drawing.Color.Black;
            this.CreateObjectButton.Location = new System.Drawing.Point(12, 132);
            this.CreateObjectButton.Name = "CreateObjectButton";
            this.CreateObjectButton.Size = new System.Drawing.Size(267, 23);
            this.CreateObjectButton.TabIndex = 1;
            this.CreateObjectButton.Text = "Create/Edit Object";
            this.CreateObjectButton.UseVisualStyleBackColor = false;
            this.CreateObjectButton.Click += new System.EventHandler(this.CreateObjectButton_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(11, 161);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(267, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Create/Edit Room";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // LoadProjectButton
            // 
            this.LoadProjectButton.BackColor = System.Drawing.Color.Black;
            this.LoadProjectButton.Location = new System.Drawing.Point(13, 12);
            this.LoadProjectButton.Name = "LoadProjectButton";
            this.LoadProjectButton.Size = new System.Drawing.Size(267, 23);
            this.LoadProjectButton.TabIndex = 4;
            this.LoadProjectButton.Text = "Load Project";
            this.LoadProjectButton.UseVisualStyleBackColor = false;
            this.LoadProjectButton.Click += new System.EventHandler(this.LoadProjectButton_Click);
            // 
            // SaveProjectButton
            // 
            this.SaveProjectButton.BackColor = System.Drawing.Color.Black;
            this.SaveProjectButton.Location = new System.Drawing.Point(13, 44);
            this.SaveProjectButton.Name = "SaveProjectButton";
            this.SaveProjectButton.Size = new System.Drawing.Size(265, 23);
            this.SaveProjectButton.TabIndex = 5;
            this.SaveProjectButton.Text = "Save Project";
            this.SaveProjectButton.UseVisualStyleBackColor = false;
            this.SaveProjectButton.Click += new System.EventHandler(this.SaveProjectButton_Click);
            // 
            // PlayProject
            // 
            this.PlayProject.BackColor = System.Drawing.Color.Black;
            this.PlayProject.Location = new System.Drawing.Point(13, 74);
            this.PlayProject.Name = "PlayProject";
            this.PlayProject.Size = new System.Drawing.Size(265, 23);
            this.PlayProject.TabIndex = 6;
            this.PlayProject.Text = "Play Project";
            this.PlayProject.UseVisualStyleBackColor = false;
            this.PlayProject.Click += new System.EventHandler(this.PlayProject_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ProjectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(292, 195);
            this.Controls.Add(this.PlayProject);
            this.Controls.Add(this.SaveProjectButton);
            this.Controls.Add(this.LoadProjectButton);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.CreateObjectButton);
            this.Controls.Add(this.button1);
            this.ForeColor = System.Drawing.Color.Lime;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ProjectForm";
            this.Text = "Project Creator";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button CreateObjectButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button LoadProjectButton;
        private System.Windows.Forms.Button SaveProjectButton;
        private System.Windows.Forms.Button PlayProject;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}