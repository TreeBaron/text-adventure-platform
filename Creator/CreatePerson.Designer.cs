﻿namespace TextAdventurePlatform
{
    partial class CreatePerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreatePerson));
            this.SelectPersonComboBox = new System.Windows.Forms.ComboBox();
            this.LoadPersonButton = new System.Windows.Forms.Button();
            this.DeletePersonButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SayUponDeathTextBox = new System.Windows.Forms.TextBox();
            this.QuestionTextBox = new System.Windows.Forms.TextBox();
            this.AnswerTextBox = new System.Windows.Forms.TextBox();
            this.AddDialogueButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DialogueComboBox = new System.Windows.Forms.ComboBox();
            this.RemoveDialogueButton = new System.Windows.Forms.Button();
            this.ProjectThingListComboBox = new System.Windows.Forms.ComboBox();
            this.GiveThingToPersonButton = new System.Windows.Forms.Button();
            this.PersonThingsComboBox = new System.Windows.Forms.ComboBox();
            this.TakeThingButton = new System.Windows.Forms.Button();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SelectPersonComboBox
            // 
            this.SelectPersonComboBox.FormattingEnabled = true;
            this.SelectPersonComboBox.Location = new System.Drawing.Point(13, 13);
            this.SelectPersonComboBox.Name = "SelectPersonComboBox";
            this.SelectPersonComboBox.Size = new System.Drawing.Size(389, 21);
            this.SelectPersonComboBox.TabIndex = 0;
            this.SelectPersonComboBox.DropDown += new System.EventHandler(this.SelectPersonComboBox_DropDown);
            this.SelectPersonComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectPersonComboBox_SelectedIndexChanged);
            // 
            // LoadPersonButton
            // 
            this.LoadPersonButton.BackColor = System.Drawing.Color.Black;
            this.LoadPersonButton.Location = new System.Drawing.Point(13, 41);
            this.LoadPersonButton.Name = "LoadPersonButton";
            this.LoadPersonButton.Size = new System.Drawing.Size(389, 23);
            this.LoadPersonButton.TabIndex = 1;
            this.LoadPersonButton.Text = "Load Person";
            this.LoadPersonButton.UseVisualStyleBackColor = false;
            this.LoadPersonButton.Click += new System.EventHandler(this.LoadPersonButton_Click);
            // 
            // DeletePersonButton
            // 
            this.DeletePersonButton.BackColor = System.Drawing.Color.Black;
            this.DeletePersonButton.Location = new System.Drawing.Point(13, 71);
            this.DeletePersonButton.Name = "DeletePersonButton";
            this.DeletePersonButton.Size = new System.Drawing.Size(389, 23);
            this.DeletePersonButton.TabIndex = 2;
            this.DeletePersonButton.Text = "Delete Person";
            this.DeletePersonButton.UseVisualStyleBackColor = false;
            this.DeletePersonButton.Click += new System.EventHandler(this.DeletePersonButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(13, 118);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(389, 20);
            this.NameTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Description";
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(13, 162);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(389, 20);
            this.DescriptionTextBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Say Upon Their Death";
            // 
            // SayUponDeathTextBox
            // 
            this.SayUponDeathTextBox.Location = new System.Drawing.Point(13, 206);
            this.SayUponDeathTextBox.Name = "SayUponDeathTextBox";
            this.SayUponDeathTextBox.Size = new System.Drawing.Size(389, 20);
            this.SayUponDeathTextBox.TabIndex = 8;
            // 
            // QuestionTextBox
            // 
            this.QuestionTextBox.Location = new System.Drawing.Point(13, 260);
            this.QuestionTextBox.Name = "QuestionTextBox";
            this.QuestionTextBox.Size = new System.Drawing.Size(389, 20);
            this.QuestionTextBox.TabIndex = 9;
            // 
            // AnswerTextBox
            // 
            this.AnswerTextBox.Location = new System.Drawing.Point(13, 299);
            this.AnswerTextBox.Name = "AnswerTextBox";
            this.AnswerTextBox.Size = new System.Drawing.Size(389, 20);
            this.AnswerTextBox.TabIndex = 10;
            // 
            // AddDialogueButton
            // 
            this.AddDialogueButton.BackColor = System.Drawing.Color.Black;
            this.AddDialogueButton.Location = new System.Drawing.Point(13, 326);
            this.AddDialogueButton.Name = "AddDialogueButton";
            this.AddDialogueButton.Size = new System.Drawing.Size(389, 23);
            this.AddDialogueButton.TabIndex = 11;
            this.AddDialogueButton.Text = "Add Dialogue";
            this.AddDialogueButton.UseVisualStyleBackColor = false;
            this.AddDialogueButton.Click += new System.EventHandler(this.AddDialogueButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 241);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Question";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Answer";
            // 
            // DialogueComboBox
            // 
            this.DialogueComboBox.FormattingEnabled = true;
            this.DialogueComboBox.Location = new System.Drawing.Point(13, 356);
            this.DialogueComboBox.Name = "DialogueComboBox";
            this.DialogueComboBox.Size = new System.Drawing.Size(389, 21);
            this.DialogueComboBox.TabIndex = 14;
            this.DialogueComboBox.DropDown += new System.EventHandler(this.DialogueComboBox_DropDown);
            // 
            // RemoveDialogueButton
            // 
            this.RemoveDialogueButton.BackColor = System.Drawing.Color.Black;
            this.RemoveDialogueButton.Location = new System.Drawing.Point(13, 384);
            this.RemoveDialogueButton.Name = "RemoveDialogueButton";
            this.RemoveDialogueButton.Size = new System.Drawing.Size(389, 23);
            this.RemoveDialogueButton.TabIndex = 15;
            this.RemoveDialogueButton.Text = "Remove Dialogue";
            this.RemoveDialogueButton.UseVisualStyleBackColor = false;
            this.RemoveDialogueButton.Click += new System.EventHandler(this.RemoveDialogueButton_Click);
            // 
            // ProjectThingListComboBox
            // 
            this.ProjectThingListComboBox.FormattingEnabled = true;
            this.ProjectThingListComboBox.Location = new System.Drawing.Point(13, 414);
            this.ProjectThingListComboBox.Name = "ProjectThingListComboBox";
            this.ProjectThingListComboBox.Size = new System.Drawing.Size(389, 21);
            this.ProjectThingListComboBox.TabIndex = 16;
            this.ProjectThingListComboBox.DropDown += new System.EventHandler(this.ProjectThingListComboBox_DropDown);
            // 
            // GiveThingToPersonButton
            // 
            this.GiveThingToPersonButton.BackColor = System.Drawing.Color.Black;
            this.GiveThingToPersonButton.Location = new System.Drawing.Point(13, 442);
            this.GiveThingToPersonButton.Name = "GiveThingToPersonButton";
            this.GiveThingToPersonButton.Size = new System.Drawing.Size(389, 23);
            this.GiveThingToPersonButton.TabIndex = 17;
            this.GiveThingToPersonButton.Text = "Give Thing To Person ";
            this.GiveThingToPersonButton.UseVisualStyleBackColor = false;
            this.GiveThingToPersonButton.Click += new System.EventHandler(this.GiveThingToPersonButton_Click);
            // 
            // PersonThingsComboBox
            // 
            this.PersonThingsComboBox.FormattingEnabled = true;
            this.PersonThingsComboBox.Location = new System.Drawing.Point(13, 472);
            this.PersonThingsComboBox.Name = "PersonThingsComboBox";
            this.PersonThingsComboBox.Size = new System.Drawing.Size(389, 21);
            this.PersonThingsComboBox.TabIndex = 18;
            this.PersonThingsComboBox.DropDown += new System.EventHandler(this.PersonThingsComboBox_DropDown);
            this.PersonThingsComboBox.SelectedIndexChanged += new System.EventHandler(this.PersonThingsComboBox_SelectedIndexChanged);
            // 
            // TakeThingButton
            // 
            this.TakeThingButton.BackColor = System.Drawing.Color.Black;
            this.TakeThingButton.Location = new System.Drawing.Point(13, 499);
            this.TakeThingButton.Name = "TakeThingButton";
            this.TakeThingButton.Size = new System.Drawing.Size(389, 23);
            this.TakeThingButton.TabIndex = 19;
            this.TakeThingButton.Text = "Take Thing From Person";
            this.TakeThingButton.UseVisualStyleBackColor = false;
            this.TakeThingButton.Click += new System.EventHandler(this.TakeThingButton_Click);
            // 
            // SubmitButton
            // 
            this.SubmitButton.BackColor = System.Drawing.Color.Black;
            this.SubmitButton.Location = new System.Drawing.Point(13, 546);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(389, 23);
            this.SubmitButton.TabIndex = 20;
            this.SubmitButton.Text = "Submit";
            this.SubmitButton.UseVisualStyleBackColor = false;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // CreatePerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(414, 581);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.TakeThingButton);
            this.Controls.Add(this.PersonThingsComboBox);
            this.Controls.Add(this.GiveThingToPersonButton);
            this.Controls.Add(this.ProjectThingListComboBox);
            this.Controls.Add(this.RemoveDialogueButton);
            this.Controls.Add(this.DialogueComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AddDialogueButton);
            this.Controls.Add(this.AnswerTextBox);
            this.Controls.Add(this.QuestionTextBox);
            this.Controls.Add(this.SayUponDeathTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeletePersonButton);
            this.Controls.Add(this.LoadPersonButton);
            this.Controls.Add(this.SelectPersonComboBox);
            this.ForeColor = System.Drawing.Color.Lime;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CreatePerson";
            this.Text = "CreatePerson";
            this.Load += new System.EventHandler(this.CreatePerson_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SelectPersonComboBox;
        private System.Windows.Forms.Button LoadPersonButton;
        private System.Windows.Forms.Button DeletePersonButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SayUponDeathTextBox;
        private System.Windows.Forms.TextBox QuestionTextBox;
        private System.Windows.Forms.TextBox AnswerTextBox;
        private System.Windows.Forms.Button AddDialogueButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox DialogueComboBox;
        private System.Windows.Forms.Button RemoveDialogueButton;
        private System.Windows.Forms.ComboBox ProjectThingListComboBox;
        private System.Windows.Forms.Button GiveThingToPersonButton;
        private System.Windows.Forms.ComboBox PersonThingsComboBox;
        private System.Windows.Forms.Button TakeThingButton;
        private System.Windows.Forms.Button SubmitButton;
    }
}