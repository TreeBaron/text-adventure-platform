﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextAdventurePlatform
{
    public partial class CreateRoom : Form
    {
        //Variables
        Room SelectedRoom = new Room();
        Thing SelectedThing = null;
        Person SelectedPerson = null;

        public CreateRoom()
        {
            InitializeComponent();
        }

        private void LoadRoomComboBox_DropDown(object sender, EventArgs e)
        {
            LoadRoomComboBox.Items.Clear();

            foreach(Room room in Project.Rooms)
            {
                LoadRoomComboBox.Items.Add(room.name);
            }
        }

        private void CreateRoom_Load(object sender, EventArgs e)
        {

        }

        private void LoadRoomButton_Click(object sender, EventArgs e)
        {
            NameTextBox.Text = SelectedRoom.name;
            DescriptionTextBox.Text = SelectedRoom.description;
            SayOnFirstEntranceTextBox.Text = SelectedRoom.sayonfirstentrance;

            if (SelectedRoom.islocked == true)
            {
                IsLockedCheckBox.Checked = true;
            }
            else
            {
                IsLockedCheckBox.Checked = false;
            }

            
                XCoordTextBox.Text = SelectedRoom.Coord.X.ToString();
                YCoordTextBox.Text = SelectedRoom.Coord.Y.ToString();
            


            if (SelectedRoom.persontodie != null)
            {
                PersonToDieComboBox.Text = SelectedRoom.persontodie.name;
            }

            if (SelectedRoom.persontochangedialogue != null)
            {
                PersonToGiveDialogueToComboBox.Text = SelectedRoom.persontochangedialogue.name;
            }

            for(int i = 0; i < SelectedRoom.Questions.Count; i++)
            {
                RemoveDialogueComboBox.Items.Add("Question: "+SelectedRoom.Questions[i]+" Answer: "+SelectedRoom.Answers[i]);
            }

            foreach(Thing thing in SelectedRoom.Things)
            {
                RoomInventoryComboBox.Items.Add(thing.name);
            }
            
            foreach(Person person in SelectedRoom.People)
            {
                RoomInventoryComboBox.Items.Add(person.name);
            }

        }

        private void LoadRoomComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach(Room room in Project.Rooms)
            {
                if (LoadRoomComboBox.SelectedText == room.name)
                {
                    SelectedRoom = room;
                }
            }
        }

        private void DeleteRoomButton_Click(object sender, EventArgs e)
        {
            foreach(Room room in Project.Rooms)
            {
                if (SelectedRoom == room)
                {
                    Project.Rooms.Remove(room);
                    LoadRoomComboBox.Text = "";
                    break;
                }
            }
        }

        private void RemoveDialogueComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RemoveDialogueComboBox.Items.Clear();

            for(int i = 0; i < SelectedRoom.Questions.Count; i++)
            {
                    RemoveDialogueComboBox.Items.Add("Question: " + SelectedRoom.Questions[i] + " Answer: " + SelectedRoom.Answers[i]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SelectedRoom.Questions.Add(QuestionTextBox.Text);
            SelectedRoom.Answers.Add(AnswerTextBox.Text);
            QuestionTextBox.Text = "";
            AnswerTextBox.Text = "";
        }

        private void RemoveDialogueButton_Click(object sender, EventArgs e)
        {
            SelectedRoom.Questions.RemoveAt(RemoveDialogueComboBox.SelectedIndex);
            SelectedRoom.Answers.RemoveAt(RemoveDialogueComboBox.SelectedIndex);
        }

        private void ProjectPeopleThingComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            foreach (Thing thing in Project.Things)
            {
                if (ProjectPeopleThingComboBox.Text == thing.name)
                {
                    SelectedThing = thing;
                }
            }

            foreach (Person person in Project.People)
            {
                if (ProjectPeopleThingComboBox.Text == person.name)
                {
                    SelectedPerson = person;
                }
            }
        }

        private void AddPersonThingButton_Click(object sender, EventArgs e)
        {
            if (SelectedPerson != null)
            {
                SelectedRoom.People.Add(SelectedPerson);
                SelectedPerson = null;
            }

            if (SelectedThing != null)
            {
                SelectedRoom.Things.Add(SelectedThing);
                SelectedThing = null;
            }

            ProjectPeopleThingComboBox.Text = "";
        }

        private void RoomInventoryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            foreach(Thing thing in SelectedRoom.Things)
            {
                RoomInventoryComboBox.Items.Add(thing.name);
            }

            foreach(Person person in SelectedRoom.People)
            {
                RoomInventoryComboBox.Items.Add(person.name);
            }
        }

        private void RemovePersonThingButton_Click(object sender, EventArgs e)
        {
            foreach(Thing thing in SelectedRoom.Things)
            {
                if (thing.name == RoomInventoryComboBox.Text)
                {
                    SelectedRoom.Things.Remove(thing);
                    RoomInventoryComboBox.Text = "";
                    break;
                }
            }

            foreach (Person person in SelectedRoom.People)
            {
                if (person.name == RoomInventoryComboBox.Text)
                {
                    SelectedRoom.People.Remove(person);
                    RoomInventoryComboBox.Text = "";
                    break;
                }
            }
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            SelectedRoom.name = NameTextBox.Text;
            SelectedRoom.description = DescriptionTextBox.Text;
            SelectedRoom.sayonfirstentrance = SayOnFirstEntranceTextBox.Text;

            if (XCoordTextBox.Text != "" && YCoordTextBox.Text != "")
            {
                try
                {
                    SelectedRoom.Coord.X = Convert.ToInt32(XCoordTextBox.Text);
                    SelectedRoom.Coord.Y = Convert.ToInt32(YCoordTextBox.Text);
                }
                catch
                {
                    MessageBox.Show("I had trouble converting your coordinates. Please make sure you include no decimals, and that your numbers are valid.");
                }
            }
            else
            {
                MessageBox.Show("The coordinate boxes have been left empty, therefore the room has no position! You may edit this later, just making sure you know you can't use this room until that get's fixed. Thanks.");
            }

            foreach(Person person in Project.People)
            {
                if (person.name == PersonToDieComboBox.Text)
                {
                    SelectedRoom.persontodie = person;
                }

                if (person.name == PersonToGiveDialogueToComboBox.Text)
                {
                    SelectedRoom.persontochangedialogue = person;
                }
            }

            if (QuestionTextBox.Text != "" && AnswerTextBox.Text != "")
            {
                SelectedRoom.Questions.Add(QuestionTextBox.Text);
                SelectedRoom.Answers.Add(AnswerTextBox.Text);

                QuestionTextBox.Text = "";
                AnswerTextBox.Text = "";
            }


            foreach (Room room in Project.Rooms)
            {
                if (room.name == SelectedRoom.name)
                {
                    Project.Rooms.Remove(room);
                    break;
                }
            }

            foreach (Thing thing in Project.Things)
            {
                if (ProjectKeyComboBox.Text == thing.name)
                {
                    SelectedRoom.Keys.Add(thing);
                }
            }

            Project.Rooms.Add(SelectedRoom);
            MessageBox.Show("Room Submitted!");
        }

        private void RemoveDialogueComboBox_DropDown(object sender, EventArgs e)
        {
            RemoveDialogueComboBox.Items.Clear();

            for (int i = 0; i < SelectedRoom.Questions.Count; i++)
            {
                RemoveDialogueComboBox.Items.Add("Question: " + SelectedRoom.Questions[i] + " Answer: " + SelectedRoom.Answers[i]);
            }
        }

        private void PersonToDieComboBox_DropDown(object sender, EventArgs e)
        {
            PersonToDieComboBox.Items.Clear();

            foreach(Person person in Project.People)
            {
                PersonToDieComboBox.Items.Add(person.name);
            }
        }

        private void PersonToGiveDialogueToComboBox_DropDown(object sender, EventArgs e)
        {
            PersonToGiveDialogueToComboBox.Items.Clear();

            foreach (Person person in Project.People)
            {
                PersonToGiveDialogueToComboBox.Items.Add(person.name);
            }
        }

        private void ProjectPeopleThingComboBox_DropDown(object sender, EventArgs e)
        {

            ProjectPeopleThingComboBox.Items.Clear();

            foreach (Thing thing in Project.Things)
            {
                ProjectPeopleThingComboBox.Items.Add(thing.name);
            }

            foreach (Person person in Project.People)
            {
                ProjectPeopleThingComboBox.Items.Add(person.name);
            }
        }

        private void RoomInventoryComboBox_DropDown(object sender, EventArgs e)
        {
            RoomInventoryComboBox.Items.Clear();

            foreach (Thing thing in SelectedRoom.Things)
            {
                RoomInventoryComboBox.Items.Add(thing.name);
            }

            foreach (Person person in SelectedRoom.People)
            {
                RoomInventoryComboBox.Items.Add(person.name);
            }
        }

        private void ProjectKeyComboBox_DropDown(object sender, EventArgs e)
        {
            ProjectKeyComboBox.Items.Clear();
            foreach(Thing thing in Project.Things)
            {
                ProjectKeyComboBox.Items.Add(thing.name);
            }
        }

        private void ProjectKeyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Thing thing in Project.Things)
            {
                ProjectKeyComboBox.Items.Add(thing.name);
            }
        }

        private void AddKeyButton_Click(object sender, EventArgs e)
        {
            foreach(Thing thing in Project.Things)
            {
                if (ProjectKeyComboBox.Text == thing.name)
                {
                    SelectedRoom.Keys.Add(thing);
                    ProjectKeyComboBox.Text = "";
                }
            }
        }

        private void RemoveKeyButton_Click(object sender, EventArgs e)
        {
            foreach (Thing thing in Project.Things)
            {
                if (ProjectKeyComboBox.Text == thing.name)
                {
                    SelectedRoom.Keys.Remove(thing);
                    ProjectKeyComboBox.Text = "";
                }
            }
        }
    }
}
