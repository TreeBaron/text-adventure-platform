﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextAdventurePlatform
{
    public partial class CreatePerson : Form
    {
        
        //Variables
        Person SelectedPerson = new Person();

        //Question and answers list
        List<string> Questions = new List<string>();
        List<string> Answers = new List<string>();
        List<Thing> Things = new List<Thing>();

        public CreatePerson()
        {
            InitializeComponent();
        }

        private void SelectPersonComboBox_DropDown(object sender, EventArgs e)
        {
            SelectPersonComboBox.Items.Clear();

            foreach(Person p in Project.People)
            {
                SelectPersonComboBox.Items.Add(p.name);
            }

        }

        private void CreatePerson_Load(object sender, EventArgs e)
        {

        }

        private void DeletePersonButton_Click(object sender, EventArgs e)
        {
            if (Project.People.Contains(SelectedPerson))
            {
                Project.People.Remove(SelectedPerson);
                SelectPersonComboBox.Text = "";
            }
        }

        private void AddDialogueButton_Click(object sender, EventArgs e)
        {
            Questions.Add(QuestionTextBox.Text);
            Answers.Add(AnswerTextBox.Text);

            QuestionTextBox.Text = "";
            AnswerTextBox.Text = "";
        }

        private void DialogueComboBox_DropDown(object sender, EventArgs e)
        {
            DialogueComboBox.Items.Clear();
            for(int i = 0; i < Questions.Count; i++)
            {
                DialogueComboBox.Items.Add("Question: "+Questions[i]+" Answer: "+Answers[i]);
            }
        }

        private void RemoveDialogueButton_Click(object sender, EventArgs e)
        {
            if (DialogueComboBox.SelectedIndex >= 0 && DialogueComboBox.SelectedIndex != null)
            {
                Questions.RemoveAt(DialogueComboBox.SelectedIndex);
                Answers.RemoveAt(DialogueComboBox.SelectedIndex);

                DialogueComboBox.Text = "";
            }
        }

        private void ProjectThingListComboBox_DropDown(object sender, EventArgs e)
        {
            ProjectThingListComboBox.Items.Clear();
            foreach(Thing thing in Project.Things)
            {
                ProjectThingListComboBox.Items.Add(thing.name);
            }
        }

        private void GiveThingToPersonButton_Click(object sender, EventArgs e)
        {
            foreach(string value in ProjectThingListComboBox.Items)
            {
                foreach(Thing thing in Project.Things)
                {
                    if (thing.name == value)
                    {
                        Things.Add(thing);
                        ProjectThingListComboBox.Text = "";
                    }
                }
            }
        }

        private void PersonThingsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PersonThingsComboBox_DropDown(object sender, EventArgs e)
        {
            PersonThingsComboBox.Items.Clear();
            foreach(Thing thing in Things)
            {
                PersonThingsComboBox.Items.Add(thing.name);
            }
        }

        private void TakeThingButton_Click(object sender, EventArgs e)
        {
            foreach(Thing thing in Things)
            {
                if (thing.name == PersonThingsComboBox.Text)
                {
                    Things.Remove(thing);
                    PersonThingsComboBox.Text = "";
                    break;
                }
            }
        }

        private void LoadPersonButton_Click(object sender, EventArgs e)
        {
            NameTextBox.Text = SelectedPerson.name;
            DescriptionTextBox.Text = SelectedPerson.description;
            SayUponDeathTextBox.Text = SelectedPerson.sayondeath;

            Questions = SelectedPerson.Questions;
            Answers = SelectedPerson.Answers;

            Things = SelectedPerson.Inventory;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            Person Temp = new Person();

            Temp.name = NameTextBox.Text;
            Temp.description = DescriptionTextBox.Text;
            Temp.sayondeath = SayUponDeathTextBox.Text;

            Temp.Answers = Answers;
            Temp.Questions = Questions;

            Temp.Inventory = Things;

            foreach(Person value in Project.People)
            {
                if(value.name == Temp.name)
                {
                    Project.People.Remove(value);
                    break;
                }
            }

            Project.People.Add(Temp);

            MessageBox.Show("Person has been created, and added to project!");
        }

        private void SelectPersonComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Person p in Project.People)
            {
                if (p.name == SelectPersonComboBox.Text)
                {
                    SelectedPerson = p;
                    break;
                }
            }
        }
    }
}
