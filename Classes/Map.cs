﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace TextAdventurePlatform
{
    [Serializable]
    public class Map
    {
        public static List<Room> Rooms = new List<Room>();
        public List<Room> SaveRooms = new List<Room>();

        public static void Save()
        {
            #region Save Map
            Map TempMap = new Map();

            TempMap.SaveRooms = Rooms;


            using(Stream stream = File.Open("data.bin", FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream,TempMap);
            }

            //Updates current room.
            Player.UpdateRoom();
            #endregion

            #region Save Player
            Player TempPlayer = new Player();
            TempPlayer.TempCoord = Player.Coord;
            TempPlayer.TempCurrentRoom = Player.CurrentRoom;
            TempPlayer.TempEquippedItem = Player.EquippedItem;
            TempPlayer.TempHealth = Player.Health;
            TempPlayer.TempName = Player.Name;
            TempPlayer.TempThings = Player.Things;
            TempPlayer.TempTurn = Player.Turn;

            using(Stream stream = File.Open("player.bin",FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream,TempPlayer);
            }
            
            #endregion
        }

        public static void Load()
        {
            #region Load Map
            Map TempMap;

            using (Stream stream = File.Open("data.bin", FileMode.Open))
            {
                BinaryFormatter bin = new BinaryFormatter();

                TempMap = (Map)bin.Deserialize(stream);

                Rooms = new List<Room>();

                foreach (Room item in TempMap.SaveRooms)
                {
                    Rooms.Add(item);
                }

                //Updates current room.
                Player.UpdateRoom();
            }
            #endregion

            #region Load Player
                Player TempPlayer = new Player();

                using(Stream stream = File.Open("player.bin",FileMode.Open))
                {

                    BinaryFormatter bin = new BinaryFormatter();

                    TempPlayer = (Player)bin.Deserialize(stream);

                    Player.Coord = TempPlayer.TempCoord;
                    Player.CurrentRoom = TempPlayer.TempCurrentRoom;
                    Player.EquippedItem = TempPlayer.TempEquippedItem;
                    Player.Health = TempPlayer.TempHealth;
                    Player.Name = TempPlayer.TempName;
                    Player.Things = TempPlayer.TempThings;
                    Player.Turn = TempPlayer.TempTurn;
                    
                }

            #endregion
            
        }
    }
}
