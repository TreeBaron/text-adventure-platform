﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventurePlatform
{
    [Serializable]
    public class Room
    {
        //Public
        public Position Coord = new Position(0,0);
        public List<Thing> Keys = new List<Thing>();
        public List<Thing> Things = new List<Thing>();
        public List<Person> People = new List<Person>();
        public List<string> Answers = new List<string>();
        public List<string> Questions = new List<string>();
        public bool FirstTimeInRoom = true;

        //Private
        private bool IsLocked = false;
        private Person PersonToDie = null;
        private Person PersonToChangeDialogue = null;
        private string SayOnFirstEntrance = null;
        private string Name;
        private string Description;
        


        #region Modifiers
        public string name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }
        public string description
        {
            get
            {
                return Description;
            }
            set
            {
                Description = value;
            }
        }
        public bool islocked
        {
            get
            {
                return IsLocked;
            }
            set
            {
                IsLocked = value;
            }
        }
        public string sayonfirstentrance
        {
            get
            {
                return SayOnFirstEntrance;
            }
            set
            {
                SayOnFirstEntrance = value;
            }
        }
       
        public void KillPersonOnEntrance(Person a)
        {
            PersonToDie = a;
        }

        public void AddDialogueToChange(string question, string answer)
        {
            Answers.Add(answer);
            Questions.Add(question);
        }
        public void SetPersonToAddDialogueTo(Person a)
        {
            PersonToChangeDialogue = a;
        }

        public Person persontodie
        {
            get
            {
                return PersonToDie;
            }
            set
            {
                PersonToDie = value;
            }
        }
        public Person persontochangedialogue
        {
            get
            {
                return PersonToChangeDialogue;
            }
            set
            {
                PersonToChangeDialogue = value;
            }
        }

        public Thing getThing(int a)
        {
            return Things.ElementAt(a);
        }
        public Person getPerson(int a)
        {
            return People.ElementAt(a);
        }
        public Thing getKey(int a)
        {
            return Keys.ElementAt(a);
        }
        public int getPersonCount()
        {
            return People.Count;
        }
        public List<Thing> getThingList()
        {
            return Things;
        }

        public void addThing(Thing a)
        {
            Things.Add(a);
        }
        public void addPerson(Person a)
        {
            People.Add(a);
        }
        public void addKey(Thing a)
        {
            Keys.Add(a);
        }

        public void removeThing(Thing a)
        {
            Things.Remove(a);
        }
        public void removePerson(Person a)
        {
            People.Remove(a);
        }
        public void removeKey(Thing a)
        {
            Keys.Remove(a);
        }

        //Method for searching containers
        public void SearchContainer()
        {
            foreach(Thing thing in Things)
            {
                if (thing.container == true && Player.EquippedItem == thing.keyobject)
                {
                    thing.Open();
                }
            }
        }

        public void DescribePlace()
        {
            Main.Edit.WriteLine(description);
            Main.Edit.AddLine();
            //Describe containers, objects, people
            foreach(Person per in People)
            {
                Main.Edit.WriteLine("You see, "+per.name+" here.");
            }
            foreach(Thing obj in Things)
            {
                Main.Edit.WriteLine("You see a "+obj.name+".");
            }
           

        }
        #endregion

        public void Update()
        {
            if (FirstTimeInRoom == true)
            {
                Main.Edit.WriteLine(SayOnFirstEntrance);
            }

            if (PersonToDie != null)
            {
                PersonToDie.Kill();
            }

            if (PersonToChangeDialogue != null)
            {
                for(int i = 0; i < Answers.Count; i++)
                {
                    PersonToChangeDialogue.AddDialogue(Questions[i],Answers[i]);
                }
            }
        }

    }
}
