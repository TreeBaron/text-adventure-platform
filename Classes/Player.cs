﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventurePlatform
{
    [Serializable]
    public class Player
    {
        //Public
        static public Position Coord = new Position(0,0);
        static public int Turn = 0;
        static public Thing EquippedItem;
        static public Room CurrentRoom = null;
        static public string Name;
        static public int Health = 100;
        static public List<Thing> Things = new List<Thing>();

        #region TempSaveVariables
        public Position TempCoord = new Position(0, 0);
        public int TempTurn = 0;
        public Thing TempEquippedItem;
        public Room TempCurrentRoom;
        public string TempName;
        public int TempHealth = 100;
        public List<Thing> TempThings = new List<Thing>();
        #endregion

        //Modifiers
        public static string name
        {
            get
            { return Name; }
            set
            { Name = value; }
        }
        public static int health
        {
            get { return Health; }
            set { Health = value; }
        }

        public static void SayPlace()
        {
            Main.Edit.WriteLine(Player.CurrentRoom.name);
            Main.Edit.WriteLine("Room Coords: " + Player.CurrentRoom.Coord.X + "," + Player.CurrentRoom.Coord.Y);
            Main.Edit.WriteLine("Player Coords: " + Player.Coord.X + "," + Player.Coord.Y);

        }

        public static void DropThing(Thing a)
        {
            if (Things.Contains(a))
            {
                if (a.name != "nothing")
                {
                    //SYNTAX!!
                    CurrentRoom.addThing(a);
                    Things.Remove(a);
                    CurrentRoom.Update();
                    Main.Edit.WriteLine(a.name + " dropped.");

                    /*
                    foreach (Thing thing in Things)
                    {
                        Form1.Edit.AddText = "PI: " + thing.name;
                    }
                    foreach (Thing thing in CurrentRoom.Things)
                    {
                        Form1.Edit.AddText = "RI: " + thing.name;
                    }*/
                }
                else
                {
                    Main.Edit.WriteLine("I must drop the nothing, but what is the nothing? You ask, waxing philosophical.");
                }
            }
        }

        public static void TakeThing(Thing a)
        {
            if (a.container == false && a.name != "nothing")
            {
                if (CurrentRoom.Things.Contains(a))
                {
                    Main.Edit.WriteLine("You take the " + a.name + ".");
                    Things.Add(a);
                    CurrentRoom.removeThing(a);

                    /*
                    foreach (Thing thing in Things)
                    {
                        Form1.Edit.AddText = "PI: " + thing.name;
                    }
                    foreach (Thing thing in CurrentRoom.Things)
                    {
                        Form1.Edit.AddText = "RI: " + thing.name;
                    }*/
                }
                else if (a.name == "nothing")
                {
                    Main.Edit.WriteLine("Why would I take nothing? Have I taken all the everything? You wonder...");
                }
            }
                           
        
        }

        public static void GoNorth()
        {

            foreach (Room Value in Map.Rooms)
            {
                if (Value.Coord.Y == Player.Coord.Y + 1)
                {
                    if (Value.islocked == false)
                    {
                        Player.Coord.Y++;
                        Player.Turn++;
                        Main.Edit.WriteLine("You walk into " + Value.name.ToLower() + ".");
                        Value.DescribePlace();
                        UpdateRoom();
                        return;
                    }
                    else
                    {
                        Main.Edit.WriteLine("You try to go that way, but it appears " + Value.name.ToLower() + " is locked!");
                        return;
                    }
                }
            }

            Main.Edit.WriteLine("You cannot go that way.");

        }
        
        public static void GoSouth()
        {
            foreach (Room Value in Map.Rooms)
            {
                if (Value.Coord.Y == Player.Coord.Y - 1)
                {
                    if (Value.islocked == false)
                    {
                        Player.Coord.Y--;
                        Player.Turn++;
                        Main.Edit.WriteLine("You walk into " + Value.name.ToLower() + ".");
                        Value.DescribePlace();
                        UpdateRoom();
                        return;
                    }
                    else
                    {
                        Main.Edit.WriteLine("You try to go that way, but it appears " + Value.name.ToLower() + " is locked!");
                        return;
                    }
                }
            }
            

            Main.Edit.WriteLine("You cannot go that way.");
        }

        public static void GoEast()
        {

            foreach (Room Value in Map.Rooms)
            {
                if (Value.Coord.X == Player.Coord.X + 1)
                {
                    if (Value.islocked == false)
                    {
                        Player.Coord.X++;
                        Player.Turn++;
                        Main.Edit.WriteLine("You walk into " + Value.name.ToLower() + ".");
                        Value.DescribePlace();
                        UpdateRoom();
                        return;
                    }
                    else
                    {
                        Main.Edit.WriteLine("You try to go that way, but it appears " + Value.name.ToLower() + " is locked!");
                        return;
                    }
                }
            }

            Main.Edit.WriteLine("You cannot go that way.");
        }

        public static void GoWest()
        {

            foreach (Room Value in Map.Rooms)
            {
                if (Value.Coord.X == Player.Coord.X - 1)
                {
                    if (Value.islocked == false)
                    {
                        Player.Coord.X--;
                        Player.Turn++;
                        Main.Edit.WriteLine("You walk into " + Value.name.ToLower() +".");
                        Value.DescribePlace();
                        UpdateRoom();
                        return;
                    }
                    else
                    {
                        Main.Edit.WriteLine("You try to go that way, but it appears " + Value.name.ToLower() + " is locked!");
                        return;
                    }
                }
            }
            

            Main.Edit.WriteLine("You cannot go that way.");
        }

        public static void Wait()
        {
            if (Player.CurrentRoom != null)
            {
                Turn++;
                Main.Edit.WriteLine("You wait for a turn, it is now turn " + Turn + ".");
                Main.Edit.WriteLine(Player.CurrentRoom.name + ":");
                Player.CurrentRoom.DescribePlace();
                return;
            }

            Main.Edit.WriteLine("You wait.");
        }

        public static void Give(Thing a)
        {
            Things.Add(a);
            Main.Edit.WriteLine("You retrieve " + a.name + ".");
        }

        public static void UpdateRoom()
        {
            foreach (Room room in Map.Rooms)
            {
                if (Player.Coord.X == room.Coord.X)
                {
                    if (Player.Coord.Y == room.Coord.Y)
                    {
                        CurrentRoom = room;
                        CurrentRoom.Update();
                        CurrentRoom.FirstTimeInRoom = false;
                    }
                }
            }
        }
    }
}
