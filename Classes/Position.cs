﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventurePlatform
{
    [Serializable]
    public class Position
    {
        public Position (int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X = 0;
        public int Y = 0;
    }
}
