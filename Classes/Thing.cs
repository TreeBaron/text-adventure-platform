﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextAdventurePlatform
{
    [Serializable]
    public class Thing
    {
        //Public
        public List<Thing> Things = new List<Thing>();
        
        //Private
        private string Name;
        private string Description;
        private bool Weapon = false;
        private bool Container = false;
        private Thing KeyObject;

        #region Modifiers
        public string name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }
        public string description
        {
            get
            {
                return Description;
            }
            set
            {
                Description = value;
            }
        }
        public Thing keyobject
        {
            get
            {
                return KeyObject;
            }
            set
            {
                KeyObject = value;
            }
        }
        public bool weapon
        {
            get
            {return Weapon;}
            set
            {
                Weapon = value;
            }
        }
        public bool container
        {
            get
            { return Container; 
            }
            set
            {
                Container = value;
            }
        }
        #endregion

        #region Mechanics
        public void Use(Thing Selected)
        {
            if (Map.Rooms.Count > 0)
            {
                if (Weapon == true)
                {
                    Room TempRoom;

                    foreach (Room Value in Map.Rooms)
                    {
                        if (Value.Coord == Player.Coord)
                        {
                            TempRoom = Value;

                            for (int i = 0; i < TempRoom.getPersonCount(); i++)
                            {
                                if (TempRoom.getPerson(i).cankill == true)
                                {
                                    TempRoom.getPerson(i).Kill();
                                }
                            }

                        }
                    }

                }

                foreach (Room Value in Map.Rooms)
                {
                    if (Value.Coord.X == Player.Coord.X + 1 || Value.Coord.X == Player.Coord.X - 1 || Value.Coord.Y == Player.Coord.Y + 1 || Value.Coord.Y == Player.Coord.Y - 1)
                    {
                        if (Value.Keys.Contains(this))
                        {
                            Value.islocked = false;
                           Main.Edit.WriteLine("Unlocked " + Value.name + " using " + Name + ".");
                        }
                    }
                }

                foreach (Thing thing in Player.CurrentRoom.Things)
                {
                    if (thing.container == true)
                    {
                        thing.Open();
                    }
                }
            }
        }
        public void Open()
        {
            if (Player.EquippedItem == KeyObject && Container == true)
            {
               Main.Edit.WriteLine("You open up the " + Name + " using " + Player.EquippedItem.name + ".");

                foreach (Thing item in Things)
                {
                    Player.Give(item);
                }

            }
            else
            {
               Main.Edit.WriteLine("You try to open up the " + Name + " using " + Player.EquippedItem.name + " but are unsuccesful.");
            }
        }


        #endregion

        
    }
}
