﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventurePlatform
{
    [Serializable]
    public class Person
    {

        //Public
        public List<string> Questions = new List<string>() {"Goodbye for now."};
        public List<string> Answers = new List<string>() {"Bye."};
        public List<Thing> Inventory = new List<Thing>();

        //Private
        private string Name;
        private string Description;
        private string SayOnDeath;
        private bool Alive = true;
        private bool CanKill = false;


        public string name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }

        public string description
        {
            get
            {
                return Description;
            }
            set
            {
                Description = value;
            }
        }

        public string sayondeath
        {
            get
            {
                return SayOnDeath;
            }
            set
            {
                SayOnDeath = value;
            }
        }

        public bool cankill
        {
            get
            {
                return CanKill;
            }
            set
            {
                CanKill = value;
            }
        }

        public void AddDialogue(string question,string answer)
        {
            Questions.Add(question);
            Answers.Add(answer);
        }

        public void RemoveDialogue(int a)
        {
            if (Questions.Count >= a)
            {
                Questions.RemoveAt(a);
                Answers.RemoveAt(a);
            }
        }

        public void Kill()
        {
            Alive = false;

            foreach(Room Value in Map.Rooms)
            {
                if (Value.People.Contains(this))
                {
                    foreach(Thing thing in Inventory)
                    {
                        Value.addThing(thing);
                    }
                }
            }

            Main.Edit.WriteLine(SayOnDeath);
            Main.Edit.WriteLine(Name + " has been killed.");
        }

        public void AddThing(Thing a)
        {
            Inventory.Add(a);
        }

        public void RemoveThing(Thing a)
        {
            Inventory.Remove(a);
        }


       

    }
}
