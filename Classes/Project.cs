﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace TextAdventurePlatform
{
    [Serializable]
    class Project
    {

        public static string FilePath = Environment.SpecialFolder.MyDocuments.ToString();
        public static string FileName = "MyProject.bin";
        public static List<Person> People = new List<Person>();
        public static List<Thing> Things = new List<Thing>();
        public static List<Room> Rooms = new List<Room>();

        public List<Person> TempPeople = new List<Person>();
        public List<Thing> TempThings = new List<Thing>();
        public List<Room> TempRooms = new List<Room>();

        public static void SaveProject()
        {
            #region Save Map

            Project MyProject = new Project();

            MyProject.TempPeople = Project.People;
            MyProject.TempRooms = Project.Rooms;
            MyProject.TempThings = Project.Things;

            using (Stream stream = File.Open(FileName, FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, MyProject);
            }

            MessageBox.Show("Project saved succesfully!");

            #endregion


        }

        public static void LoadProject()
        {
            try
            {
                Project MyProject = new Project();

                using (Stream stream = File.Open(FileName, FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();

                    MyProject = (Project)bin.Deserialize(stream);

                }

                Project.People = MyProject.TempPeople;
                Project.Things = MyProject.TempThings;
                Project.Rooms = MyProject.TempRooms;

                MessageBox.Show("Project Loaded Succesfully!");
            }
            catch
            {
                MessageBox.Show("There was a problem loading your save file! Please check your file path.");
                
            }

        }

    }
}
