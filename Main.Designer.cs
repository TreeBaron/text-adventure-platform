﻿namespace TextAdventurePlatform
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SayButton = new System.Windows.Forms.Button();
            this.ComboBoxPlayerInventory = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.WaitButton = new System.Windows.Forms.Button();
            this.WestButton = new System.Windows.Forms.Button();
            this.GoNorthButton = new System.Windows.Forms.Button();
            this.SouthButton = new System.Windows.Forms.Button();
            this.EastButton = new System.Windows.Forms.Button();
            this.DropButton = new System.Windows.Forms.Button();
            this.UseButton = new System.Windows.Forms.Button();
            this.EquipButton = new System.Windows.Forms.Button();
            this.InspectButton = new System.Windows.Forms.Button();
            this.LookButton = new System.Windows.Forms.Button();
            this.TakeButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ComboBoxRoomItems = new System.Windows.Forms.ComboBox();
            this.SaveGameButton = new System.Windows.Forms.Button();
            this.LoadGameButton = new System.Windows.Forms.Button();
            this.CreatorButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ControlText;
            this.textBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(12, 516);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(638, 20);
            this.textBox1.TabIndex = 0;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ControlText;
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.White;
            this.richTextBox1.Location = new System.Drawing.Point(12, 33);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(537, 475);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // SayButton
            // 
            this.SayButton.BackColor = System.Drawing.Color.Black;
            this.SayButton.Location = new System.Drawing.Point(656, 514);
            this.SayButton.Name = "SayButton";
            this.SayButton.Size = new System.Drawing.Size(74, 23);
            this.SayButton.TabIndex = 2;
            this.SayButton.Text = "Say";
            this.SayButton.UseVisualStyleBackColor = false;
            this.SayButton.Click += new System.EventHandler(this.SayButton_Click);
            // 
            // ComboBoxPlayerInventory
            // 
            this.ComboBoxPlayerInventory.FormattingEnabled = true;
            this.ComboBoxPlayerInventory.Location = new System.Drawing.Point(555, 56);
            this.ComboBoxPlayerInventory.Name = "ComboBoxPlayerInventory";
            this.ComboBoxPlayerInventory.Size = new System.Drawing.Size(174, 21);
            this.ComboBoxPlayerInventory.TabIndex = 3;
            this.ComboBoxPlayerInventory.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            this.ComboBoxPlayerInventory.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(555, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Inventory";
            // 
            // WaitButton
            // 
            this.WaitButton.BackColor = System.Drawing.Color.Black;
            this.WaitButton.Location = new System.Drawing.Point(606, 417);
            this.WaitButton.Name = "WaitButton";
            this.WaitButton.Size = new System.Drawing.Size(44, 40);
            this.WaitButton.TabIndex = 5;
            this.WaitButton.Text = "Wait";
            this.WaitButton.UseVisualStyleBackColor = false;
            this.WaitButton.Click += new System.EventHandler(this.WaitButton_Click);
            // 
            // WestButton
            // 
            this.WestButton.BackColor = System.Drawing.Color.Black;
            this.WestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WestButton.Location = new System.Drawing.Point(556, 417);
            this.WestButton.Name = "WestButton";
            this.WestButton.Size = new System.Drawing.Size(44, 40);
            this.WestButton.TabIndex = 6;
            this.WestButton.Text = "W";
            this.WestButton.UseVisualStyleBackColor = false;
            this.WestButton.Click += new System.EventHandler(this.WestButton_Click);
            // 
            // GoNorthButton
            // 
            this.GoNorthButton.BackColor = System.Drawing.Color.Black;
            this.GoNorthButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GoNorthButton.Location = new System.Drawing.Point(606, 371);
            this.GoNorthButton.Name = "GoNorthButton";
            this.GoNorthButton.Size = new System.Drawing.Size(44, 40);
            this.GoNorthButton.TabIndex = 7;
            this.GoNorthButton.Text = "N";
            this.GoNorthButton.UseVisualStyleBackColor = false;
            this.GoNorthButton.Click += new System.EventHandler(this.GoNorthButton_Click);
            // 
            // SouthButton
            // 
            this.SouthButton.BackColor = System.Drawing.Color.Black;
            this.SouthButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SouthButton.Location = new System.Drawing.Point(606, 463);
            this.SouthButton.Name = "SouthButton";
            this.SouthButton.Size = new System.Drawing.Size(44, 40);
            this.SouthButton.TabIndex = 8;
            this.SouthButton.Text = "S";
            this.SouthButton.UseVisualStyleBackColor = false;
            this.SouthButton.Click += new System.EventHandler(this.SouthButton_Click);
            // 
            // EastButton
            // 
            this.EastButton.BackColor = System.Drawing.Color.Black;
            this.EastButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EastButton.Location = new System.Drawing.Point(656, 417);
            this.EastButton.Name = "EastButton";
            this.EastButton.Size = new System.Drawing.Size(44, 40);
            this.EastButton.TabIndex = 9;
            this.EastButton.Text = "E";
            this.EastButton.UseVisualStyleBackColor = false;
            this.EastButton.Click += new System.EventHandler(this.EastButton_Click);
            // 
            // DropButton
            // 
            this.DropButton.BackColor = System.Drawing.Color.Black;
            this.DropButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DropButton.Location = new System.Drawing.Point(559, 149);
            this.DropButton.Name = "DropButton";
            this.DropButton.Size = new System.Drawing.Size(165, 39);
            this.DropButton.TabIndex = 10;
            this.DropButton.Text = "Drop";
            this.DropButton.UseVisualStyleBackColor = false;
            this.DropButton.Click += new System.EventHandler(this.DropButton_Click);
            // 
            // UseButton
            // 
            this.UseButton.BackColor = System.Drawing.Color.Black;
            this.UseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UseButton.Location = new System.Drawing.Point(559, 194);
            this.UseButton.Name = "UseButton";
            this.UseButton.Size = new System.Drawing.Size(165, 39);
            this.UseButton.TabIndex = 11;
            this.UseButton.Text = "Use";
            this.UseButton.UseVisualStyleBackColor = false;
            this.UseButton.Click += new System.EventHandler(this.UseButton_Click);
            // 
            // EquipButton
            // 
            this.EquipButton.BackColor = System.Drawing.Color.Black;
            this.EquipButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EquipButton.Location = new System.Drawing.Point(559, 239);
            this.EquipButton.Name = "EquipButton";
            this.EquipButton.Size = new System.Drawing.Size(165, 39);
            this.EquipButton.TabIndex = 12;
            this.EquipButton.Text = "Equip";
            this.EquipButton.UseVisualStyleBackColor = false;
            this.EquipButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // InspectButton
            // 
            this.InspectButton.BackColor = System.Drawing.Color.Black;
            this.InspectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InspectButton.Location = new System.Drawing.Point(559, 284);
            this.InspectButton.Name = "InspectButton";
            this.InspectButton.Size = new System.Drawing.Size(165, 39);
            this.InspectButton.TabIndex = 13;
            this.InspectButton.Text = "Inspect";
            this.InspectButton.UseVisualStyleBackColor = false;
            this.InspectButton.Click += new System.EventHandler(this.InspectButton_Click);
            // 
            // LookButton
            // 
            this.LookButton.BackColor = System.Drawing.Color.Black;
            this.LookButton.Location = new System.Drawing.Point(657, 485);
            this.LookButton.Name = "LookButton";
            this.LookButton.Size = new System.Drawing.Size(73, 23);
            this.LookButton.TabIndex = 14;
            this.LookButton.Text = "Look";
            this.LookButton.UseVisualStyleBackColor = false;
            this.LookButton.Click += new System.EventHandler(this.LookButton_Click);
            // 
            // TakeButton
            // 
            this.TakeButton.BackColor = System.Drawing.Color.Black;
            this.TakeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeButton.Location = new System.Drawing.Point(559, 329);
            this.TakeButton.Name = "TakeButton";
            this.TakeButton.Size = new System.Drawing.Size(165, 39);
            this.TakeButton.TabIndex = 15;
            this.TakeButton.Text = "Take";
            this.TakeButton.UseVisualStyleBackColor = false;
            this.TakeButton.Click += new System.EventHandler(this.TakeButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(555, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 24);
            this.label2.TabIndex = 17;
            this.label2.Text = "Room Items";
            // 
            // ComboBoxRoomItems
            // 
            this.ComboBoxRoomItems.FormattingEnabled = true;
            this.ComboBoxRoomItems.Location = new System.Drawing.Point(555, 107);
            this.ComboBoxRoomItems.Name = "ComboBoxRoomItems";
            this.ComboBoxRoomItems.Size = new System.Drawing.Size(174, 21);
            this.ComboBoxRoomItems.TabIndex = 16;
            this.ComboBoxRoomItems.DropDown += new System.EventHandler(this.ComboBoxRoomItems_DropDown);
            this.ComboBoxRoomItems.SelectedIndexChanged += new System.EventHandler(this.ComboBoxRoomItems_SelectedIndexChanged);
            // 
            // SaveGameButton
            // 
            this.SaveGameButton.BackColor = System.Drawing.Color.Black;
            this.SaveGameButton.Location = new System.Drawing.Point(13, 4);
            this.SaveGameButton.Name = "SaveGameButton";
            this.SaveGameButton.Size = new System.Drawing.Size(75, 23);
            this.SaveGameButton.TabIndex = 18;
            this.SaveGameButton.Text = "Save Game";
            this.SaveGameButton.UseVisualStyleBackColor = false;
            this.SaveGameButton.Click += new System.EventHandler(this.SaveGameButton_Click);
            // 
            // LoadGameButton
            // 
            this.LoadGameButton.BackColor = System.Drawing.Color.Black;
            this.LoadGameButton.Location = new System.Drawing.Point(94, 4);
            this.LoadGameButton.Name = "LoadGameButton";
            this.LoadGameButton.Size = new System.Drawing.Size(75, 23);
            this.LoadGameButton.TabIndex = 19;
            this.LoadGameButton.Text = "Load Game";
            this.LoadGameButton.UseVisualStyleBackColor = false;
            this.LoadGameButton.Click += new System.EventHandler(this.LoadGameButton_Click);
            // 
            // CreatorButton
            // 
            this.CreatorButton.BackColor = System.Drawing.Color.Black;
            this.CreatorButton.Location = new System.Drawing.Point(176, 3);
            this.CreatorButton.Name = "CreatorButton";
            this.CreatorButton.Size = new System.Drawing.Size(75, 23);
            this.CreatorButton.TabIndex = 20;
            this.CreatorButton.Text = "Creator";
            this.CreatorButton.UseVisualStyleBackColor = false;
            this.CreatorButton.Click += new System.EventHandler(this.CreatorButton_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.SayButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(736, 549);
            this.Controls.Add(this.CreatorButton);
            this.Controls.Add(this.LoadGameButton);
            this.Controls.Add(this.SaveGameButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ComboBoxRoomItems);
            this.Controls.Add(this.TakeButton);
            this.Controls.Add(this.LookButton);
            this.Controls.Add(this.InspectButton);
            this.Controls.Add(this.EquipButton);
            this.Controls.Add(this.UseButton);
            this.Controls.Add(this.DropButton);
            this.Controls.Add(this.EastButton);
            this.Controls.Add(this.SouthButton);
            this.Controls.Add(this.GoNorthButton);
            this.Controls.Add(this.WestButton);
            this.Controls.Add(this.WaitButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxPlayerInventory);
            this.Controls.Add(this.SayButton);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBox1);
            this.ForeColor = System.Drawing.Color.Lime;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Text Adventure Platform";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button SayButton;
        private System.Windows.Forms.ComboBox ComboBoxPlayerInventory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button WaitButton;
        private System.Windows.Forms.Button WestButton;
        private System.Windows.Forms.Button GoNorthButton;
        private System.Windows.Forms.Button SouthButton;
        private System.Windows.Forms.Button EastButton;
        private System.Windows.Forms.Button DropButton;
        private System.Windows.Forms.Button UseButton;
        private System.Windows.Forms.Button EquipButton;
        private System.Windows.Forms.Button InspectButton;
        private System.Windows.Forms.Button LookButton;
        private System.Windows.Forms.Button TakeButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboBoxRoomItems;
        private System.Windows.Forms.Button SaveGameButton;
        private System.Windows.Forms.Button LoadGameButton;
        private System.Windows.Forms.Button CreatorButton;
    }
}

